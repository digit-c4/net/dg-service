# Contributing

## Personal Access Token

To start working on this project, you will have to first create a personal access Token

Go on your profil > preferences > personal access token and create your token. You will be able to see this token only once so put it in your keypass.

This token will be used to connect to git when doing a **git pull** or **git push**

## First steps when working on this project

First thing you have to do is getting the code from this repo on your local machine.

Create a directory in your home vm. You can access your VM by ssh lab-yoursername

**cd** without option will get you directly to your home repo

**mkdir name_of_dir** to create ur local dir

You will have to setup your proxy on Linux to communicate with gitlab, ask the devs that already worked on this project for that.

once in your directory, do a **git clone https://code.europa.eu/digit-c4/net/dg-service.git**

This will bring all the files on your local directory.

## Working with git

**git branch -a** to check the list of branches and your current branch, * means your current branch

**git checkout branch_name** to switch to a specific branch

**git remote -v** to check the list of the remote servers from where you are pulling and pushing the code (setup automatically by the **git clone**)

**origin** is the name of the remote git server.

The workflow that you should adopt when modifying some code on git is the following:

first, you pull the code from the remote repo.

while being on the main branch: **git pull**. In case someone has pushed changes, you get the last updates.

Then you change to a new branch before doing your modifications:

**git checkout -b new_branch** git checkout allows the switch of branch but with **-b** it also create a branch at the same time.

then you do your modifications,

you then do a **git add "filename"** to add the file to the staging area

**git commit -m "your msg"** to commit the file in the staging area. It will put the file in the local repo. A commit will just save a new version of the file in the git history, but for now this is only done in local, we then need to push it to the remote repo (code europa)

You can't push directly to the main branch, you need to push to a new branch first, and then do what we call a PR (Pull Request) to then do the merge from the source branch that you created to the main branch.

**git push origin branch_name** to push your local modifications to the remote repo.

You will see on git that you have now a blue button that asks you to do the merge request.

You click on it, you assign the PR to another member of the team that will review your code and accept or not your merge request. once its accepted, the code will be put in the main branch and the source branch will be deleted.

Then, you have some cleaning left to do on your repo, switch back to the main branch

**git checkout main**

delete the branch you created

**git branch --delete your_branch_name**

do a **git pull** once again to get the change you just pushed remotely on your local (because you will not have the merged main on your local).

Repeat this workflow for everytime you have something to change or new code to add.

---

- name: Validate CSV File for vPC ACI Static Path Binding ACI
  hosts: localhost
  connection: local
  gather_facts: no
  vars:
    template_tags: "{{ 
        'static_path_binding_vpc_add_validation_fail' if awx_webhook_payload.data.name == 'static_path_binding_vpc_add' else
        'static_path_binding_vpc_delete_validation_fail' if awx_webhook_payload.data.name == 'static_path_binding_vpc_delete' else
        'static_path_binding_vpc_query_validation_fail' if awx_webhook_payload.data.name == 'static_path_binding_vpc_query' else
        'default_validation_fail' }}"

  tasks:

    - name: Install python package
      ansible.builtin.pip:
        name:
          - pynetbox 
          - jsonschema

    - name: Split CSV data and extract values Extract values 
      include_role: 
        name: netbox_input_validation
        tasks_from: csv_format.yml
    
    - name: Convert specific fields to integers in each CSV record
      ansible.builtin.set_fact:
        json_output: "{{ json_output | default([]) + [item | combine({'leaf1_id': (item.leaf1_id | int),
          'leaf2_id': (item.leaf2_id | int),
          'interface_id': (item.interface_id | int), 
          'pod_id': (item.pod_id | int),
          'encap_id': (item.encap_id | int) })] }}"
      loop: "{{ csv_data }}"
    
    - name: Interogate Netbox
      include_role:
        name: netbox_interogation
        tasks_from: main.yml
    
    - name: Validate the DC location
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_dc_location.yml
    
    - name: Update config template if verification fails for DC location
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ dc_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"

    - name: End Play if template is updated and validation fails for DC location
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ dc_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
    
    - name: Validate the environment
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_environment.yml
    
    - name: Update config template if verification fails for the environment 
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ env_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for the environment 
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ env_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the leaf IDs for vPC pair (Should be a number of length 4)
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_leaf_id_vpc.yml
    
    - name: Update config template if verification fails for leaf IDs for vPC pair
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ leaf_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
      
    - name: End Play if template is updated for leaf IDs for vPC pair if verification fails
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ leaf_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the interface ID (Should be only the port number without the slot in front)
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_interface_id_vpc.yml
      vars:
        fail_condition: "{{ interface_id_vpc_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
    
    - name: Update config template if verification fails for the interface ID
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ interface_id_vpc_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for the interface ID
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ interface_id_vpc_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the application profile
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_ap.yml
    
    - name: Update config template if verification fails for application profile
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ aplication_profile_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for application profile
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ aplication_profile_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
    
    - name: Validate the VLAN encapsulation ID
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_vlan_id.yml
    
    - name: Update config template if verification fails for VLAN encapsulation ID
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ encap_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for VLAN encapsulation ID
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ encap_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the POD
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_pod.yml
    
    - name: Update config template if verification fails for POD ID
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ pod_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name:  End Play if template is updated and validation fails for POD ID
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ pod_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the switchport_mode
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_switchport_mode.yml
    
    - name: Update config template if verification fails for the switchport_mode
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ switchport_mode_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for the switchport_mode
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ switchport_mode_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Read JSON schema
      ansible.builtin.set_fact:
        json_schema: "{{ lookup('file', 'json_static_path_binding_vpc_validation.json') | from_json }}"
    
    - name: Update config template if verification fails for reading the JSON schema
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ json_schema == None or json_schema | length == 0 }}"
        template_update_tags: "{{ template_tags }}"

    - name: End Play if template is updated and validation fails for reading the JSON schema
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ json_schema == None or json_schema | length == 0 }}"
    
    - name: Validate config context json data against the schema
      ansible.utils.validate:
        data: "{{ json_output }}"
        criteria: "{{ json_schema }}"
        engine: ansible.utils.jsonschema
      ignore_errors: True
      register: validation_result
  
    - name: Display different messages based on validation result
      ansible.builtin.debug:
        msg: |
          {% if validation_result.failed == False %}
          Validation successful - Applying config context to the device in NetBox
          {% else %}
          Validation failed - Skipping config context update
          {% endif %}
    
    - name: Update config template based on validation result
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ validation_result.failed == True or validation_result.failed == False }}"
        template_update_tags: 
          "{{
            ['static_path_binding_vpc_add_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'static_path_binding_vpc_add' else
            ['static_path_binding_vpc_add_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'static_path_binding_vpc_add' else
            ['static_path_binding_vpc_delete_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'static_path_binding_vpc_delete' else
            ['static_path_binding_vpc_delete_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'static_path_binding_vpc_delete' else
            ['static_path_binding_vpc_query_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'static_path_binding_vpc_query' else
            ['static_path_binding_vpc_query_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'static_path_binding_vpc_query' else
            ['default_validation_fail']
          }}"
    
    - name: End Play if template is updated and validation fails for validating config context json data against the schema
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ validation_result.failed == True }}"
      
    - name: Update config context of a device in NetBox
      include_role:
        name: netbox_configuration
        tasks_from: update_local_config_context.yml
      vars:
        fail_condition: "{{ validation_result.failed == False }}"




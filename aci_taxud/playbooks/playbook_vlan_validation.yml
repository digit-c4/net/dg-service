---

- name: Validate CSV File for VLAN ACI Creation
  hosts: localhost
  connection: local
  gather_facts: no
  vars:
    template_tags: "{{ 
          'vlan_add_validation_fail' if awx_webhook_payload.data.name == 'interface_nonred_add' else
          'vlan_delete_validation_fail' if awx_webhook_payload.data.name == 'interface_nonred_delete' else
          'vlan_query_validation_fail' if awx_webhook_payload.data.name == 'interface_nonred_query' else
          'default_validation_fail' }}"
  tasks:
    
    - name: Install python package
      ansible.builtin.pip:
        name:
          - pynetbox 
          - jsonschema

    - name: Split CSV data and extract values Extract values 
      include_role: 
        name: netbox_input_validation
        tasks_from: csv_format.yml
    
    - name: Convert specific fields to integers in each CSV record
      ansible.builtin.set_fact:
        json_output: "{{ json_output | default([]) + [item | combine({'encap_id': (item.encap_id| int) })] }}"
      loop: "{{ csv_data }}"

    - name: Interogate Netbox
      include_role:
        name: netbox_interogation
        tasks_from: main.yml
    
    - name: Validate the application profile
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_ap.yml
    
    - name: Update config template if verification fails for application profile
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ aplication_profile_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for application profile
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ aplication_profile_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the vlan description  
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_vlan_description.yml
    
    - name: Update config template if verification fails for vlan description
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ description_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for vlan description
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ description_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
    
    - name: Validate the VLAN encapsulation ID
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_vlan_id.yml
    
    - name: Update config template if verification fails for VLAN encapsulation ID
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ encap_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for VLAN encapsulation ID
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ encap_id_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Validate the subnet
      include_role: 
        name: netbox_input_validation
        tasks_from: validate_subnet.yml
    
    - name: Update config template if verification fails for subnet
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ subnet_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"
        template_update_tags: "{{ template_tags }}"
    
    - name: End Play if template is updated and validation fails for subnet
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ subnet_result.results | selectattr('failed', 'equalto', True) | list | length > 0 }}"

    - name: Read JSON schema
      ansible.builtin.set_fact:
        json_schema: "{{ lookup('file', 'json_vlan_validation.json') | from_json }}"
    
    - name: Update config template if verification fails for reading the JSON schema
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ json_schema == None or json_schema | length == 0 }}"
        template_update_tags: "{{ template_tags }}"

    - name: End Play if template is updated and validation fails for reading the JSON schema
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ json_schema == None or json_schema | length == 0 }}"
    
    - name: Validate config context json data against the schema
      ansible.utils.validate:
        data: "{{ json_output }}"
        criteria: "{{ json_schema }}"
        engine: ansible.utils.jsonschema
      ignore_errors: True
      register: validation_result
  
    - name: Display different messages based on validation result
      ansible.builtin.debug:
        msg: |
          {% if validation_result.failed == False %}
          Validation successful - Applying config context to the device in NetBox
          {% else %}
          Validation failed - Skipping config context update
          {% endif %}
    
    - name: Update config template based on validation result
      include_role:
        name: netbox_configuration
        tasks_from: update_config_template.yml
      vars:
        template_update_condition: "{{ validation_result.failed == True or validation_result.failed == False }}"
        template_update_tags: 
          "{{
            ['vlan_add_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'vlan_add' else
            ['vlan_add_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'vlan_add' else
            ['vlan_delete_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'vlan_delete' else
            ['vlan_delete_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'vlan_delete' else
            ['vlan_query_validation_fail'] if validation_result.failed == True and awx_webhook_payload.data.name == 'vlan_query' else
            ['vlan_query_validation_pass'] if validation_result.failed == False and awx_webhook_payload.data.name == 'vlan_query' else
            ['default_validation_fail']
          }}"
    
    - name: End Play if template is updated and validation fails for validating config context json data against the schema
      include_role:
        name: netbox_input_validation
        tasks_from: fail_playbook.yml
      vars:
        fail_condition: "{{ validation_result.failed == True }}"
      
    - name: Update config context of a device in NetBox
      include_role:
        name: netbox_configuration
        tasks_from: update_local_config_context.yml
      vars:
        fail_condition: "{{ validation_result.failed == False }}"

